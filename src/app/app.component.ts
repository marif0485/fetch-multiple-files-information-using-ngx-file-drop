import { Component, ChangeDetectorRef } from '@angular/core';
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'app works!';
  public files: NgxFileDropEntry[] = [];
  public data: Array<any> = [];

  private units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];

  constructor(private ref: ChangeDetectorRef) {}

  public dropped(files: NgxFileDropEntry[]) {
    if (files.length > 6) {
      alert('Cannot add more than 6 Files at a time.');
    } else {
      this.data = [];
      let index = 0;
      this.files = [];
      for (const droppedFile of files) {
        // Is it a file?
        if (
          droppedFile.fileEntry.isFile &&
          this.isFileAllowed(droppedFile.fileEntry.name)
        ) {
          const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
          fileEntry.file((file: File) => {
            if (this.isFileSizeAllowed(file.size)) {
              //files array is used to display
              //the files to the user which will be uploaded
              this.files.push(droppedFile);
              if (this.files.length < 6) {
                // Here you can access the real file
                let modifiedDate = new Date(file.lastModified).toLocaleString();
                this.data.push({
                  name: this.files[index].relativePath,
                  size: this.transform(file.size),
                  modified: modifiedDate,
                });
                index++;
                // this.uploadFile(file);
              } else {
                alert('Maximum 6 files are allowed.');
              }
            } else {
              alert(
                'Max size of a file allowed is 25 MB, files with size more than 1mb are discarded.'
              );
            }
          });
        } else {
          index++;
          alert(
            "Only files in '.pdf', '.jpg', '.jpeg', '.png', '.mp4' format are accepted and directories are not allowed."
          );
        }
      }
    }
  }

  
  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    const allowedFiles = ['.pdf', '.jpg', '.jpeg', '.png', '.mp4'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    return isFileAllowed;
  }

  isFileSizeAllowed(size) {
    let isFileSizeAllowed = false;
    if (size < 26214400) {
      isFileSizeAllowed = true;
    }
    return isFileSizeAllowed;
  }
  transform(bytes: number = 0, precision: number = 2): string {
    if (isNaN(parseFloat(String(bytes))) || !isFinite(bytes)) return '?';

    let unit = 0;

    while (bytes >= 1024) {
      bytes /= 1024;
      unit++;
    }

    return bytes.toFixed(+precision) + ' ' + this.units[unit];
  }

  public fileOver(event) {
  }

  public fileLeave(event) {
  }

  uploadFile(file) {
    /*
    // You could upload it like this:
    const formData = new FormData();
    formData.append('logo', file, relativePath);

    // Headers
    const headers = new HttpHeaders({
      'security-token': 'mytoken',
    });

    this.http
      .post(
        'https://mybackend.com/api/upload/sanitize-and-save-logo',
        formData,
        { headers: headers, responseType: 'blob' }
      )
      .subscribe((data) => {
        // Sanitized logo returned from backend
      });
      */
  }

  // public onChangeTable(
  //   config: any,
  //   page: any = { page: this.page, itemsPerPage: this.itemsPerPage }
  // ): any {
  //   // let filteredData = this.changeFilter(this.files, this.config);
  //   this.rows =
  //     page && config.paging ? this.changePage(page, this.data) : this.data;
  //   this.length = this.files.length;
  // }

  // public changePage(page: any, data: Array<any>): Array<any> {
  //   let start = (page.page - 1) * page.itemsPerPage;
  //   let end = page.itemsPerPage > -1 ? start + page.itemsPerPage : data.length;
  //   return data.slice(start, end);
  // }
}
